from time import sleep
import json
import socket
import sys

from gameparse import *
from pathfinding import *

if len(sys.argv) > 1:
    host = sys.argv[1]
    print "Connecting to: " + host + ":54321"

else:
    host = socket.gethostname()

    print "Usage: python2.7 bigsinep.py remote-addr\n"
    print "Connecting to: localhost:54321"

# Connect to socket
s = socket.socket()
port = 54321

s.connect((host, port))

# Init bot
s.send(b"NAME BigSinep\n")
s.send(b"JSON\n")

while True:
    try:
        # State recieved == action start.
        game_state = json.loads(s.recv(8192))

        if game_state and game_state['type'] == 'status update':

            # TODO: Wrapper class

            """Parse information."""
            command = b"SAY I did nothing!"

            x, y = game_state['x'], game_state['y']

            bomb_map = parse_bombs(game_state['bombs'], game_state['map'])
            player_map = parse_players(game_state['players'])

            game_map = parse_map(game_state['map'])

            """Decisions."""
            # -- Check bomb safety
            # -- -- Evade bombs
            if in_danger((x, y), bomb_map, player_map, game_state['map']):
                safe_paths = get_safe_paths(
                    (x, y),
                    bomb_map,
                    player_map,
                    game_state['map']
                )

                if safe_paths:
                    print str(len(safe_paths)) + " solutions."
                    print "Using " + str(safe_paths[0])

                    command = safe_paths[0][1][0]

                else:
                    command = b"SAY Shiiiieeeet!\n"

            # -- Check self trap safety
            # -- -- Never walk into a place where there is no escape

            # -- Check distance and availibility to other players
            # -- -- Pursue players
            # -- -- Push them into corner, lay bomb
            # availible_players = set()
            # for player in game_state['players']:
            #     px, py = player['x'], player['y']
            #     walk = aStar(game_map[(x, y)], game_map[(px, py)])

            #     if walk:
            #         availible_players.add(walk)
                    # command = walk[0]

            # -- Go camping if only one enemy left

            # -- Execute command
            s.send(command)

            """Show debug map."""
            path = None
            for player in game_state['players']:
                px, py = player['x'], player['y']

                path = aStar(game_map[(x, y)], game_map[(px, py)], path_trace)

            height, width = len(game_state['map']), len(game_state['map'][0])
            for y in range(0, height):
                for x in range(0, width):
                    if (x, y) in game_map:
                        if (game_state['x'], game_state['y']) == (x, y):
                            print 'S',

                        elif (x, y) in player_map:
                            print 'P',

                        elif (x, y) in bomb_map['locations']:
                            print 'B',

                        elif (x, y) in bomb_map:
                            print bomb_map[(x, y)][0],

                        elif path and game_map[(x, y)] in path:
                            print '*',

                        else:
                            print ' ',

                    else:
                        print '+',
                print

    except ValueError, e:
        print "ERROR:" + str(e)

s.close()