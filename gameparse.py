from pathfinding import Node


def parse_map(game_map):
    """Return graph representation of game map.
    Used with astar.
    """
    parsed_map = {}
    height, width = len(game_map), len(game_map[0])

    for y in range(0, height):
        for x in range(0, width):
            current = game_map[y][x]

            if current != '+' and current != '#':
                node = Node(x, y)

                if x > 0:
                    if (x-1, y) in parsed_map:
                        neighbour = parsed_map[(x-1, y)]
                        node.neighbours.add(neighbour)
                        neighbour.neighbours.add(node)

                if y > 0:
                    if (x, y-1) in parsed_map:
                        neighbour = parsed_map[(x, y-1)]
                        node.neighbours.add(neighbour)
                        neighbour.neighbours.add(node)

                parsed_map[(x, y)] = node

    return parsed_map


def parse_bombs(bombs, game_map):
    """Return dict of bomb blasts and bomb locations.
    For easy lookup of bombs.
    """
    # Sort bombs by number of ticks left until explosion
    # so that they are easier to parse if they hit eachother.
    bombs.sort(key=lambda bomb: bomb['state'])

    bomb_map = {}
    bomb_map['locations'] = set()

    height, width = len(game_map), len(game_map[0])

    def add_blast(location, state):
        """Add a bomb blast to a location in bomb_map.
        Multiple blasts are put in a sorted list.
        """
        x, y = location
        if (0 < x < width and 0 < y < height):

            if game_map[y][x] != '.':
                # Blasts to be avoided will only exist on free tiles.
                return

            # Add bomb location to the map
            if location in bomb_map:
                bomb_map[location].append(state)
                bomb_map[location].sort()

            else:
                bomb_map[location] = [state]

    for bomb in bombs:
        x, y = bomb['x'], bomb['y']

        if (x, y) in bomb_map:
            # A bomb might be hit by another
            # bomb, thus prematurely exploding it
            bomb['state'] = bomb_map[(x, y)][0]

        bomb_map['locations'].add((x, y))

        # Estimate bomb blast
        add_blast((x-2, y), bomb['state'])
        add_blast((x-1, y), bomb['state'])
        add_blast((x+1, y), bomb['state'])
        add_blast((x+2, y), bomb['state'])
        add_blast((x, y-2), bomb['state'])
        add_blast((x, y-1), bomb['state'])
        add_blast((x, y+1), bomb['state'])
        add_blast((x, y+2), bomb['state'])

    return bomb_map


def parse_players(players):
    """Return dictionary of player locations.
    Each location containing the id of the player.
    """
    player_map = {}

    for player in players:
        x, y = player['x'], player['y']
        player_map[(x, y)] = player['id']

    return player_map