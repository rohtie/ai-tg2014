Artifical Intelligence competition - The Gathering 2014
=======================================================

I made this for the AI compo at The Gathering where the AI bots fight against eachother in a bomberman game.

As the checklist below suggest, I wasn't able to finish the bot, so that it could lay bombs. 

However it was very good at avoiding bombs, so it almost got into the final rounds 

because it was good at survival and often was the last standing bomberman. 

Unfortunately, it is only possible to get points if you bomb stuff, so I didn't win.


::

	[X] Basic pathfinding using A*
	[X] Follow player using pathfinding
	[X] Evade bombs
	[ ] Avoid getting trapped
	[ ] Trap enemies and bomb them
	[ ] Camping
	[ ] Say funny stuff if we are in real shit