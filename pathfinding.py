from Queue import Queue
import random


class Node:
    """Node representing a walkable tile."""
    def __init__(self, x, y):
        self.parent = None
        self.neighbours = set()

        self.x = x
        self.y = y

        self.g = 0
        self.f = 0

    def __repr__(self):
        return ("node(%d, %d - %d)" % (self.x, self.y, len(self.neighbours)))


def manhattan(a, b):
    """Manhattan heurisitc."""
    return abs(a.x-b.x) + abs(a.y-b.y)


def path_trace(nodes):
    """Trace A* path using directional commands.
    Used for path debug.
    """
    if nodes[0].parent:
        return path_trace([nodes[0].parent] + nodes)

    return nodes


def walk_path(nodes):
    """Trace A* path using directional commands."""
    def sub_walk(node):
        if node.parent:
            if node.x < node.parent.x:
                return sub_walk(node.parent) + [b"LEFT\n"]
            if node.x > node.parent.x:
                return sub_walk(node.parent) + [b"RIGHT\n"]
            if node.y < node.parent.y:
                return sub_walk(node.parent) + [b"UP\n"]
            if node.y > node.parent.y:
                return sub_walk(node.parent) + [b"DOWN\n"]
        return []

    return sub_walk(nodes[0])


def aStar(start, target, trace=walk_path):
    """Astar implementation with manhattan heuristics."""
    visited = set()
    free_nodes = set([start])

    start.g = 0
    start.f = start.g + manhattan(start, target)

    while free_nodes:
        current = min(free_nodes, key=lambda node: node.f)

        if current == target:
            return trace([target])

        free_nodes.remove(current)
        visited.add(current)

        for neighbour in current.neighbours:
            if neighbour not in visited or current.g + 1 < neighbour.g:
                    neighbour.parent = current
                    neighbour.g = current.g + 1
                    neighbour.f = current.g + manhattan(neighbour, target)

                    if neighbour not in free_nodes:
                        free_nodes.add(neighbour)

    return False


def in_danger(position, bomb_map, players, game_map):
    """Returns True if the we are within bomb range."""
    if position in bomb_map:
        return True

    return False


def potential_trap(self_position, bomb_map, players, game_map):
    """Make sure we never walk into a trap.
    BFS, looking for ourselves.
    Return True, if it is a trap.
    """
    pass


def traverse_safe_paths(q):
    """Return all the safe paths as walkable commands, sorted after risk factor.
    Used to evade bombs.
    Not recursive because I wanted some variety, also no TCO in python.
    """
    paths = []

    while not q.empty():
        current = q.get()

        risk = 0

        path = []
        while current['parent']:
            x, y = current['position']
            px, py = current['parent']['position']

            if current['risk']:
                # Risk assessment - lower is better
                risk += current['risk']

            if (x, y) == (px, py):
                path.append(random_statement())

            elif x < px:
                path.append(b"LEFT\n")
            elif x > px:
                path.append(b"RIGHT\n")
            elif y < py:
                path.append(b"UP\n")
            elif y > py:
                path.append(b"DOWN\n")

            current = current['parent']

        path.reverse()
        paths.append((risk, path))

    paths.sort(key=lambda path: path[0])

    return paths


def space_risk(position, bombs, players, game_map, count_limit=2):
    """We want to be in a position with the most space possible.
    BFS.

    Return number of obstacles in our surrounding tiles.
    """
    height, width = len(game_map), len(game_map[0])

    visited = set()

    q = Queue()
    q.put((0, position))

    risk = 0

    def add_pos(count, position):
        """Add a tile to the queue to be traversed."""
        if position not in visited:
            visited.add(position)
            q.put((count + 1, position))

    while not q.empty():
        current = q.get()
        count, position = current
        x, y = position

        if (0 < x < width and 0 < y < height):
            if (position in bombs['locations'] or
               position in players or
               game_map[y][x] != '.'):
                risk += 1

        if count != count_limit:
            add_pos(count, (x + 1, y))
            add_pos(count, (x - 1, y))
            add_pos(count, (x, y + 1))
            add_pos(count, (x, y - 1))

    return risk/7.0


def get_safe_paths(position, bombs, players, game_map, max_count=3):
    """Calculate potential safe paths.
    Return safe paths ordered by risk.
    Variant of BFS without elimination of visited nodes.
    """
    height, width = len(game_map), len(game_map[0])

    q = Queue()
    q.put({'count': 0,
           'position': position,
           'parent': None,
           'bomb_path': False})

    def add_tile(position, current):
        """Add a tile to the queue to be traversed."""

        x, y = position

        risk = 0
        if (0 < x < width and 0 < y < height):
            if position in bombs:
                for bomb in bombs[position]:
                    if current['count'] + 1 == bomb:
                        # We are in a bomb path that will explode
                        # when we walk there, therefore do not continue
                        return

                risk = len(bombs[position])

            risk += space_risk(position, bombs, players, game_map)

            q.put({'count': current['count'] + 1,
                   'position': position,
                   'parent': current,
                   'risk': risk})

    while not q.empty():
        current = q.get()

        x, y = current['position']

        tile = game_map[y][x]

        # Return paths
        if current['count'] == max_count:
            return traverse_safe_paths(q)

        # Add potential safe location for current tick
        if ((x, y) not in bombs['locations'] and
           (x, y) not in players and tile == '.'):

            add_tile((x, y), current)
            add_tile((x-1, y), current)
            add_tile((x+1, y), current)
            add_tile((x, y-1), current)
            add_tile((x, y+1), current)

    print traverse_safe_paths(q)
    return None


def random_statement():
    """Return a random statement, that we can say in times of trouble."""

    statements = [
        b"SAY Well this is awkward...\n",
        b"SAY I wonder what Sinep is backwards\n",
        b"SAY ^^;\n",
        b"SAY What? Was I supposed to do something?\n",
        b"SAY Don't stare, I am dangerous\n",
        b"SAY Helicopter!\n",
        b"SAY Derp\n",
        b"SAY Noooooooooooooo, I am father\n",
        b"SAY Do not want\n",
    ]

    random.shuffle(statements)

    return statements[0]